/*
** timer.c for project in /home/vincent/skynavion/soft/libTIMER
** 
** Made by Vincent Davoust
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sat Dec 14 09:34:48 2013 Vincent Davoust
** Last update Sun Jan 10 13:11:18 2016 Vincent Davoust
** 
** Prefix : TMR
*/

#include "timer.h"


void	TMR_calcPrediv(uint8_t bits, uint32_t microseconds,\
		       uint8_t *prediv, uint16_t *ocr) {
  uint32_t	maxFlag;
  uint8_t	predivs[5] = {0, 3, 6, 8, 10}; /* predivs, en puissances de 2 */
  uint8_t	i;

  /* Verifications */
  if ((bits != 8 && bits != 16) | !prediv | !ocr)
    return ;

  maxFlag = (bits = 16)? 0xFFFF : 0xFF; /* maximum de ocr, selon nb de bits */

  /* Adaptation de microsecondes par rapport a la frequence cpu en MHz */
  microseconds = microseconds << 2;

  /* Essais avec les differents prediviseurs */
  for (i = 0; i < 5; i++) {
    if ((microseconds >> predivs[i]) < maxFlag) {
      *prediv = i;
      *ocr = (microseconds >> predivs[i]);
      if (maxFlag == 0xFF) {
	*ocr = *ocr << 8;	/* decalage pour aller dans OCR0A (!OCR0B) */
      }
      return ;
    }
  }

  /* Cas ou aucune configuration ne marche : mettre le max */
  *ocr = maxFlag;
  *prediv = 5;
}

void	TMR_nop() {
  return ;
}
