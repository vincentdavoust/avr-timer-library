/*
** timer.h for project in /home/vincent/skynavion/soft/libTIMER
** 
** Made by Vincent Davoust
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sat Dec 14 09:34:58 2013 Vincent Davoust
** Last update Sun Jan 10 18:38:34 2016 Vincent Davoust
** 
** Prefix : TMR
*/

#ifndef TIMER_H_
# define TIMER_H_

#ifdef SKYNAVION_V2
#include "../env/skynavion_v2.h"
#else
#include "../libraries.h"
#endif

void	TMR_calcPrediv(uint8_t bits, uint32_t microsseconds,\
		       uint8_t *prediv, uint16_t *ocr);
void	TMR_nop();

#endif /* !TIMER_H_ */
