/*
** timer0.h for project in /home/vincent/skynavion/soft/libTIMER
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sat May 17 22:57:18 2014 Vincent
** Last update Sun Jan 10 13:07:04 2016 Vincent Davoust
**
** Prefix : TMR0
*/

#ifndef TIMER0_H_
# define TIMER0_H_

#include "timer.h"

void	TMR0_init_default(uint32_t microseconds);
void	TMR0_init(uint8_t tccr0a, uint8_t tccr0b,\
		  uint16_t ocr0, uint8_t timsk);
void	TMR0_stop();
void	TMR0_restart(uint32_t microseconds);
void	(*TMR0_interrupt)();
void	TMR0_attachInt(void (*func)());
void	TMR0_int(void (*func)(), uint32_t micros);

#endif /* !TIMER0_H_ */
