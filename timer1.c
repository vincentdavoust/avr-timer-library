/*
** timer0.c for project in /home/vincent/skynavion/soft/libTIMER
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Sat May 17 22:57:32 2014 Vincent
** Last update Thu Jan  7 10:09:46 2016 Vincent Davoust
**
** Prefix : TMR0
*/


#include <avr/io.h>
#include "timer0.h"


ISR (TIMER0_COMPA_vect) {
  TMR0_interrupt();
}

// main use function : start timer and call func on overflow reach
void	TMR0_int(uint32_t micros, void (*func)()) {
  TMR0_init_default(micros);
  TMR0_attachInt(func);
}

void		TMR0_init_default(uint32_t microseconds) {
  uint8_t	prediv, tccr0a, timsk;
  uint16_t	ocr0;

  /* default for tccr0a is 16 bits, wafeform generation */
  tccr0a = (1 << TCW0) | (1 << CTC0);
  /* Calculating predi & occr0 */
  TMR_calcPrediv(16, microseconds, &prediv, &ocr0);
  /* default for timsk is interrupt on ocr0 match*/
  timsk = timsk | (1 << OCIE0A);
  TMR_init0(tccr0a, prediv, ocr0, timsk);
}

void	TMR0_init(uint8_t tccr0a, uint8_t tccr0b,\
		  uint16_t ocr0, uint8_t timsk) {
  TCNT0L = 0;				/* Remise a 0 du compteur */
  TCNT0H = 0;
  TCCR0A = tccr0a;			/* Set to requested value */
  TCCR0B = tccr0b;			/* set to requester value */
  OCR0A = ocr0 >> 8;			/* MSB of ocr0 */
  OCR0B = ocr0 & 0xFF;			/* LSB of ocr0 */
  TIMSK = (TIMSK & 0xE4) | timsk;	/* Set only timer0 bits of TIMSK */
}

void	TMR0_stop() {
  TCCR0B = TCCR0B & (~(7 << CS00));	/* dissable clock source */
}

void	TMR0_restart(uint32_t microseconds) {
  uint8_t	prediv;
  uint16_t	ocr0;

  /* Calculating predi & occr0 */
  TMR_calcPrediv(16, microseconds, &prediv, &ocr0);
  OCR0A = ocr0 >> 8;			/* MSB of ocr0 */
  OCR0B = ocr0 & 0xFF;			/* LSB of ocr0 */
  TCCR0B = TCCR0B | prediv;		/* set to requester value */
}

void	TMR0_attachInt(void (*func)()) {
  TMR0_interrupt = func;
}
